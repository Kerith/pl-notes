Virtual Machine (analogous to JVM)
==========================

The basic structure contains:

- A Stack of partial results
- A program counter
- The bytecode of the program

Memory
-------------

We can add a memory to add binding and referencing variables.
The memory is shaped as a cell grid, or linear array. We can say that each VariableDeclaration will have a cell in the grid. Then, it's just a matter of traversing the AST and assign a space to every VariableDeclaration and set it to NULL -> Another Processing.

**CHANGES TO BE MADE:** Furthermore, instead of VariableReference storing strings, IT SHOULD ONLY STORE A POINTER TO THE VariableDeclaration!!!! The same with the Assignment.

E.G:
Bytecode for `z = z * 4;`
would be:   
  ```
    apilaInt(4);
    apilaDir(2); // Push the value of Mem[2] (the address of z)
    mul;
    desapilaDir(2); // Pop into the address of z
  ```

  We deal with trying to access NULL memory values by pushing `unknown`. Then, the operations that have `unknown` as a parameter will also be unknown.
