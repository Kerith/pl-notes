COnstructed Types: structs and arrays
===========================

We need to define two more **Operations**:
- `[ ]` to access an array
- `.` to access the fields of a struct (`x.myField`).

## Memory interface
Before we only had VaraibleReferences, now we need to abstract it to "Memory Access", and VaraibleReference will be a subclass. ArrayIndexReference and StructFieldReference will also be subclasses.

## `[ ]`

Needs:
 - A memory element (A reference to an array decalration) and
 - an Index, which is an "Expression" that returns an int.

```
The type is the same of the elements of the array:
    [ ]
     |   \
Mem(t0)  Exp(t1)

Where t0 is TArray(SIZE) of elements of type T
and t1 is of type int
Then [ ] will have a type T
```

## `.`
Needs:
- A list of FieldDeclarations
- A reference to the desired field.

The type of the `.` expression will be the same as the required type.

## Type Declarations

How do we represent:
```c++
typedef struct {
  int x;
  int y;
} Point;

typedef struct {
  int radius;
  Point center;
} Circle;

Circle c;
```

Circle is a `TypeDeclaration("Circle")` statement, with type:
TRecord, with fields:
  - DField("radius"), with fields   
    - int
  - DField("center"), with fields
    - TRef("Point") A TRef is a reference to a `TypeDeclaration` statement.

The same with Point
For `Circle c;`, we have a `VariableDeclaration("c")`, which has a declared type TRef("Circle"), similar to that of the DField("center")

The type inference is done by following the TRef chain until we get to an atomic declaration.
