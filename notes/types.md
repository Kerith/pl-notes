Type Systems
=============

The main idea is to define a grammar for the type of each expression.
The type of an expression is determined by it's parameters and return value:

```
// Constants have their own terminal types
CtInt -> int
CteBool -> bool

// Composite types
And -> if (op1 == bool and op2 == bool) => bool
    -> else => Error

Add -> if (op1 == int and op2 == int) => int
    -> else => Error
```

Then, each node in the AST has type annotations, and **static type-checking** is just **A post-order traversal** of the AST.

Of course, each node has to carry additional information, such as:
* The type annotation
* The localization information: Usually line numbers and columns.

It is usually a good idea to process the _entire_ AST when type checking, to catch all possible errors.

The error notifications should not be redundant. That is, we should **only notify an error if all the operands have been defined**.

```
&&
|  \
+   +    => Notify 2 errors, one for each +
|\  |\
5 T 6 T
```

Implementation
---------------------

An enum is fine for small languages, but we need more robust systems: A class system.

* Each type is a subclass of `Type`: `Int`, `Bool`, `Error`...
* Each `Exp` has an instance to a `Type` subclass.

Variable Declaration
-------------------------

When a variable decalration is stored, it's type must be stored and referenciable in the environment. That is, an ASSIGNMENT expression must link to the VariableDeclaration node, so that the type checker can check that the types are the same. The same with VariableReference, it must also have a pointer to it's VaraibleDeclaration.

TypeCheck in a block
-----------------------

A block is OK if all it's expressions have type OK (the expressions can have either OK or Error type).
