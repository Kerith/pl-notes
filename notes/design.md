Design Patterns in Interpreter Implementation
=================================

Interpreter Pattern (Smart nodes)
---------------------
One solution for the Giant Switches problem is to have each expression perform it's own functions, so for examples the `Exp` class would have a `print()` abstract method, and each subclass would implement it.

Problems:
* The traversal rules are hard-coded and **scrambled across classes**.
* If we want to add additional traversals (e.g. type checking), we have to touch in many places.

Visitor Pattern (Dumb nodes)
--------------------
Another solution is to have an abstract class just dedicated to the traversal of the AST, let's call it `Visitor`. `Visitor` has an `visit()` method for each subclass of `Exp`. Each subclass of `Exp` must have an `accept()` method, usually with this body:

```java
public void accept(Visitor v) {
  v.process(this);
}
```

It has to be in **each concrete subclass** because we want to call the correct method in the `Visitor`.

This way, each subclass of `Visitor` will define a different traversal of the tree, so that we might have `TypeCheckVisitor`, `EvaluateVisitor`... operating over the **same AST**.

This gives the advantage of the **locality of the processing**.
