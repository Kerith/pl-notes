package program;

import processing.Processing;

public abstract class Program {

  public interface Exp {
    void accept(Processing p);
  }

  public class Int implements Exp {
    private int val;
    public Int(int val) {
      this.val = val;
    }

    // Accessors for the processing
    public int value() {return val;}

    // Accept method for the processing
    public void accept(Processing p) {
      p.process(this);
    }
  }

  public class Bool implements Exp {
    private boolean val;
    public Bool(boolean val) {
      this.val = val;
    }

    // Accessors for the processing
    public boolean value() {return val;}

    // Accept method for the processing
    public void accept(Processing p) {
      p.process(this);
    }
  }

  private abstract class BinExp implements Exp {
    protected Exp op1;
    protected Exp op2;
    public BinExp(Exp op1, Exp op2) {
      this.op1 = op1;
      this.op2 = op2;
    }

    // Accessors for the processing
    public Exp op1() {return op1;}
    public Exp op2() {return op2;}
  }

  public class Add extends BinExp {
    public Add(Exp op1, Exp op2) {
      super(op1, op2);
    }

    // Accept method for the processing
    public void accept(Processing p) {
      p.process(this);
    }
  }

  public class And extends BinExp {
    public And(Exp op1, Exp op2) {
      super(op1, op2);
    }

    // Accept method for the processing
    public void accept(Processing p) {
      p.process(this);
    }
  }

  // Aux methods for demonstration purpouses
  public abstract Exp root();
  public static Int cint(int val) { return new Int(val); }
  public static Bool cbool(boolean val) { return new Bool(val); }
  public static Add add(Exp op1, Exp op2) { return new Add(op1, op2); }
  public static And and(Exp op1, Exp op2) { return new And(op1, op2); }

}
