package processing.print;

import program.Program;

public class PrintTest extends Program {
  // Just as an example
  public Exp root() {
    return and(add(cint(5), cint(6)), cbool(true)));
  }

  public static void main(String[] args) {
    PrintTest program = new PrintTest();
    Print print = new Print();

    print.process(program.root());
  }
}
