package processing;

import program.Program.Int;
import program.Program.Bool;
import program.Program.Add;
import program.Program.And;


public class Processing {
  public void process(Int exp) {}
  public void process(Bool exp) {}
  public void process(Add exp) {}
  public void process(And exp) {}
}
