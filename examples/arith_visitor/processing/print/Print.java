package processing.print;
import processing.Processing;
import program.Program;

public class Print extends Processing {
  public void process(Int exp) {
    System.out.print(exp.value());
  }
  public void process(Bool exp) {
    System.out.print(exp.value());
  }
  public void process(Add exp) {
    // Recursive call to accept
    System.out.print('(');
    exp.op1().accept(this);
    System.out.print(" + ");
    exp.op2().accept(this);
    System.out.print(')');
  }
  public void process(And exp) {
    System.out.print('(');
    exp.op1().accept(this);
    System.out.print(" && ");
    exp.op2().accept(this);
    System.out.print(')');
  }

  public void process(TInt exp) {}
  public void process(TBool exp) {}
  public void process(TError exp) {}
}
