package tests;

import program.Program;

public class PrintTest extends Program {
  private Exp program;

  public Test(Exp program) {
    this.program = and(add(cint(5), cint(6)), cbool(true)));
  }

  public static void main(String[] args) {
    TypeCheck tcheck = new TypeCheck(program);
    Print print = new Print(true); // Print attributes

    program.accept(tcheck);
    program.accept(print);
  }
}
