package processing.print;
import processing.Processing;

import program.Program;
import program.Program.Int;
import program.Program.Bool;
import program.Program.Add;
import program.Program.And;

import proram.Program.TInt;
import proram.Program.TBool;
import proram.Program.TError;

public class TypeCheck extends Processing {
  private Program program;

  public TypeCheck(Program program) {
    this.program = program;
  }

  public void process(Int exp) {
    exp.setType(program.tint())
  }
  public void process(Bool exp) {
    exp.setType(program.tbool());
  }
  public void process(Add exp) {
    exp.op1().accept(this);
    exp.op2().accept(this);
    if (exp.op1().type().equals(program.tint()) &&
        exp.op2.type().equals(program.tint())) {
          exp.setType(program.tint());
        } else {
          exp.setType(program.terror());
        }
  }

  public void process(And exp) {
    exp.op1().accept(this);
    exp.op2().accept(this);
    if (exp.op1().type().equals(program.tbool()) &&
        exp.op2.type().equals(program.tbool())) {
          exp.setType(program.tbool());
        }  else {
          exp.setType(program.terror());
        }
  }
}
