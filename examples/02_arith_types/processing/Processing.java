package processing;

import program.Program.Int;
import program.Program.Bool;
import program.Program.Add;
import program.Program.And;

import proram.Program.TInt;
import proram.Program.TBool;
import proram.Program.TError;

public class Processing {
  public void process(Int exp) {}
  public void process(Bool exp) {}
  public void process(Add exp) {}
  public void process(And exp) {}

  public void process(TInt exp) {}
  public void process(TBool exp) {}
  public void process(TError exp) {}
}
