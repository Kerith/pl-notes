package processing.print;
import processing.Processing;

import program.Program;
import program.Program.Int;
import program.Program.Bool;
import program.Program.Add;
import program.Program.And;

import proram.Program.TInt;
import proram.Program.TBool;
import proram.Program.TError;

public class Print extends Processing {
  private boolean printAttributes;

  public Print() {
    printAttributes = false;
  }

  public Print(bool printAttributes) {
    this.printAttributes = printAttributes;
  }

  private void printAttributes(Exp p) {
    if (this.printAttributes) {
      System.out.print("{t:" + p.type() + "}");
    }
  }

  public void process(Int exp) {
    System.out.print(exp.value());
  }
  public void process(Bool exp) {
    System.out.print(exp.value());
  }
  public void process(Add exp) {
    // Recursive call to accept
    System.out.print('(');
    exp.op1().accept(this);
    System.out.print(" + ");
    exp.op2().accept(this);
    System.out.print(')');
  }
  public void process(And exp) {
    System.out.print('(');
    exp.op1().accept(this);
    System.out.print(" && ");
    exp.op2().accept(this);
    System.out.print(')');
  }
}
