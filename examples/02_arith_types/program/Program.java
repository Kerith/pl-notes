package program;

import processing.Processing;

public abstract class Program {

  private TInt tInt = new TInt();
  private TBool tBool= new TBool();
  private TError tError = new TError();

  public Programa() {
    tInt = new TInt();
    tBool= new TBool();
    tInt = new TError();
  }

  // TYPE SYSTEM
  // --------------
  public interface Type {
    void accept(Processing p);
  }

  public class TInt implements Type {
    public void accept(Processing p) {
      p.process(this);
    }

    public String toString() { return "T_INT";}
  }

  public class TBool implements Type {
    public void accept(Processing p) {
      p.process(this);
    }

    public String toString() { return "T_BOOL";}
  }

  public class TError implements Type {
    public void accept(Processing p) {
      p.process(this);
    }
    
    public String toString() { return "T_ERROR";}
  }


  // EXPRESSION NODES
  // ----------------
  public abstract class Exp {
    private Type type;
    public Exp() {
      this.type = null;
    }

    public void setType(Type t) {
      this.type = type;
    }

    public void type() {
      return this.type;
    }

    void accept(Processing p);
  }

  public class Int extends Exp {
    private int val;
    public Int(int val) {
      this.val = val;
    }

    // Accessors for the processing
    public int value() {return val;}

    // Accept method for the processing
    public void accept(Processing p) {
      p.process(this);
    }
  }

  public class Bool extends Exp {
    private boolean val;
    public Bool(boolean val) {
      this.val = val;
    }

    // Accessors for the processing
    public boolean value() {return val;}

    // Accept method for the processing
    public void accept(Processing p) {
      p.process(this);
    }
  }

  private abstract class BinExp extends Exp {
    protected Exp op1;
    protected Exp op2;
    public BinExp(Exp op1, Exp op2) {
      this.op1 = op1;
      this.op2 = op2;
    }

    // Accessors for the processing
    public Exp op1() {return op1;}
    public Exp op2() {return op2;}
  }

  public class Add extends BinExp {
    public Add(Exp op1, Exp op2) {
      super(op1, op2);
    }

    // Accept method for the processing
    public void accept(Processing p) {
      p.process(this);
    }
  }

  public class And extends BinExp {
    public And(Exp op1, Exp op2) {
      super(op1, op2);
    }

    // Accept method for the processing
    public void accept(Processing p) {
      p.process(this);
    }
  }

  // Aux methods for demonstration purpouses
  public abstract Exp root();

  public Int cint(int val) { return new Int(val); }
  public Bool cbool(boolean val) { return new Bool(val); }
  public Add add(Exp op1, Exp op2) { return new Add(op1, op2); }
  public And and(Exp op1, Exp op2) { return new And(op1, op2); }

  public Type tint() { return this.tInt; }
  public Type tbool() { return this.tBool; }
  public Type terror() { return this.tError; }
}
